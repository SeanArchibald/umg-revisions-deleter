=== UMG Revisions Deleter ===
Contributors: Sean Archibald
Tags: revisions
Requires at least: 4.1.5
Tested up to: 4.4.4
Stable tag: 4.4.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin lets you bulk-delete revisions manually, or automatically upon login.

== Description ==

When you have 10,000s of post revisions, you don't want to delete them all at once and hammer your WordPress database for hours on end. This plugin lets you delete a number of revisions at a time, to gradually reduce your revision count. Optionally, the revisions deletion can happen automatically each time an admin logs into the site.

Settings > UMG Revisions Deleter

== Installation ==

You can use UMG Revisions either as a plugin or as a theme include.

= As a plugin =

1. Upload the plugin files to the `/wp-content/plugins/umg-revisions-deleter` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->UMG Revisions Deleter page to configure the plugin

= As a theme include =

1. Upload umg-revisions-deleter.php to the `/wp-content/themes/your-theme` directory.
1. Add the following code to your functions.php: `include_once( 'umg-revisions-deleter.php' );`
1. Use the Settings->UMG Revisions Deleter page to configure the plugin

== Changelog ==

= 0.9 =
* First release.

== Upgrade Notice ==

= 0.9 =
Nothing special.
