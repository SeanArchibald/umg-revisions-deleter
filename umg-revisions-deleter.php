<?php
/*
Plugin Name: UMG Revisions Deleter
Description: Deletes some revisions each time a user logs in.
Author: Push Entertainment | Sean Archibald
Author URI: http://pushentertainment.com
*/

defined( 'ABSPATH' ) or die( 'Cannot access this page directly.' );
if ( defined( DOING_AJAX ) && DOING_AJAX ) { return; }


if ( !class_exists( 'UmgDeleteRevisions' ) ) {

	class UmgDeleteRevisions {

		public static $num_to_del;
		const defaults = array(
			'num_to_del'			=> '50',
			'num_to_keep'			=> '5',
			'delete_on_login'	=> '0',
		);

		function __construct() {

			if ( get_option( 'delete_on_login', $defaults['delete_on_login'] ) ) {
				add_action( 'wp_login', array( $this, 'deleteRevisions' ), 999 );
			}

			if( isset($_GET['umgDeleteRevisions']) ) {
				add_action( 'admin_init', function() { self::deleteRevisions();	}, 20 );
				add_action( 'shutdown', function() { echo '<script>window.location = "' . get_site_url() . '/wp-admin/options-general.php?page=umg-revisions-deleter";</script>'; }, 999 );
			}

			add_action( 'admin_menu', array( $this, 'pluginMenu' ) );
			add_action( 'admin_init', array( $this, 'pluginOptionsInit' ) );
			add_filter( 'wp_revisions_to_keep', array( $this, 'keepRevisions' ) );

		}

		function keepRevisions( $num ) {

			return get_option( 'num_to_keep', $defaults['num_to_keep'] );

		}

		function pluginMenu() {
			add_options_page( 'UMG Revisions Deleter', 'UMG Revisions Deleter', 'activate_plugins', 'umg-revisions-deleter', array( $this, 'pluginOptions' ) );
		}

		function pluginOptionsInit() {

			register_setting( 'umg-revisions-deleter-group', 'num_to_del' );
			register_setting( 'umg-revisions-deleter-group', 'num_to_keep' );
			register_setting( 'umg-revisions-deleter-group', 'delete_on_login' );

			add_settings_section( 'settings_section', 'Settings', array( $this, 'settings_section_callback' ), 'umg-revisions-deleter-group' );
			add_settings_field( 'num_to_del', 'Number of revisions to delete per login', array( $this, 'num_to_del_callback' ), 'pluginMenu', 'settings_section' );
			add_settings_field( 'num_to_keep', 'Revisions to keep', array( $this, 'num_to_keep_callback' ), 'pluginMenu', 'settings_section' );
			add_settings_field( 'delete_on_login', 'Delete on login', array( $this, 'delete_on_login_callback' ), 'pluginMenu', 'settings_section' );

		}

		function settings_section_callback(){
		}

		function pluginOptions() {

			if ( !current_user_can( 'activate_plugins' ) ) {
				return;
			}

			global $wpdb;
			$rev_count = $wpdb->get_var( "SELECT COUNT(ID) FROM $wpdb->posts WHERE post_type = 'revision'" );

			?><div class="wrap">
					<h1>UMG Revisions Deleter</h1>
					<p>Current revisions count: <?php echo $rev_count; ?></p>
					<?php if ( 0 != get_option( 'num_to_del', $defaults['num_to_del'] ) ) { ?><p><a class="button" href="<?php echo get_site_url(); ?>/wp-admin/options-general.php?page=umg-revisions-deleter&umgDeleteRevisions">Delete <?php echo get_option( 'num_to_del', $defaults['num_to_del'] ); ?> revision<?php if ( 1 != get_option( 'num_to_del', $defaults['num_to_del'] ) ) echo 's'; ?> right now</a></p><?php } ?>

					<form method="post" action="options.php" style="margin-top:3em;">
						<?php	settings_fields( 'umg-revisions-deleter-group' );
							do_settings_sections( 'umg-revisions-deleter-group' );	?>
						<table class="form-table">
							<tr valign="top">
								<th scope="row"><label for="delete_on_login">Auto-delete on login</label></th>
								<td><input type="checkbox" name="delete_on_login" id="delete_on_login" <?php if ( get_option( 'delete_on_login', $defaults['delete_on_login'] ) ) echo 'checked="checked"'; ?> /></td>
							</tr>
	            <tr valign="top">
                <th scope="row"><label for="num_to_del">Revisions to delete</label></th>
                <td><input type="number" min="0" max="10000" name="num_to_del" id="num_to_del" value="<?php echo get_option( 'num_to_del', $defaults['num_to_del'] ); ?>" /></td>
	            </tr>
							<tr valign="top">
								<th scope="row"><label for="num_to_keep">Revisions to keep</label></th>
								<td><input type="number" min="-1" max="10000" name="num_to_keep" id="num_to_keep" value="<?php echo get_option( 'num_to_keep', $defaults['num_to_keep'] ); ?>" /></td>
							</tr>
	        	</table>
						<?php	submit_button(); ?>
					</form>
				</div>
			<?php

		}

		function setNumToDel( $n ) {
			if ( ctype_digit( $n ) ) {
				self::$num_to_del = intval( $n );
			}
		}

		function getNumToDel() {
			return self::$num_to_del;
		}

		static function deleteRevisions( $login = null ) {

			self::setNumToDel( get_option( 'num_to_del', $defaults['num_to_del'] ) );

			if ( $login ) {
				$user = get_user_by( 'login', $login );
				$uid = $user->ID;
			}
			else {
				$uid = get_current_user_id();
			}

			if ( function_exists(user_can) ) {
				if ( !user_can( $uid, 'edit_posts') ) {
					return;
				}
			}

			if ( !is_int( self::getNumToDel() ) || self::getNumToDel() < 1 ) {
				return;
			}

			global $wpdb;

			$revisions = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_type = 'revision' ORDER BY post_date ASC LIMIT " . self::$num_to_del );

			if ( !empty( $wpdb->last_error ) ) {

				error_log( $wpdb->last_error );
				return;

			}

			$num_deleted = 0;

			foreach ( $revisions as $rev ) {

				$result = wp_delete_post_revision( $rev->ID );

				if ( $result != null && !is_wp_error( $result ) ) {
					$num_deleted++;
				}

			}

			//error_log( $num_deleted . ' revisions deleted' );

		}

	}

	new UmgDeleteRevisions();

}
